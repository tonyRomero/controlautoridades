import { Component, OnInit } from '@angular/core';
import { Service } from '../rest-services.service';

@Component({
  selector: 'app-fusion-registros',
  templateUrl: './fusion-registros.component.html',
  styleUrls: ['./fusion-registros.component.css']
})
export class FusionRegistrosComponent implements OnInit {

  usuarios : Promise <any[]>;

  constructor( private service : Service ) { }

  ngOnInit() {

    this.service.getUsers();
    console.log (this.usuarios);

  }
}
