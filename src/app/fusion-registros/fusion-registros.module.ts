import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FusionRegistrosComponent } from './fusion-registros.component';


@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
        {
            path: '',
            component: FusionRegistrosComponent 
          }
      ])

  ],
  declarations: [FusionRegistrosComponent],

  providers : [],
})
export class FusionRegistrosModule {

}