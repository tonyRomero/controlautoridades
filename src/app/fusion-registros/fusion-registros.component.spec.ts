import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FusionRegistrosComponent } from './fusion-registros.component';

describe('FusionRegistrosComponent', () => {
  let component: FusionRegistrosComponent;
  let fixture: ComponentFixture<FusionRegistrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FusionRegistrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FusionRegistrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
