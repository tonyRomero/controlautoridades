import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PorcentajesComponent } from './porcentajes.component';

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
        {
            path: '',
            component: PorcentajesComponent
          }
      ])

  ],
  declarations: [PorcentajesComponent],

  providers : [],
})
export class PorcentajesModule {

}