import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProduccionComponent } from './produccion.component';

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
        {
            path: '',
            component: ProduccionComponent
          }
      ])

  ],
  declarations: [ProduccionComponent],

  providers : [],
})
export class ProduccionModule {

}