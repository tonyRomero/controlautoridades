import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinMovimientosComponent } from './sin-movimientos.component';

describe('SinMovimientoComponent', () => {
  let component: SinMovimientosComponent;
  let fixture: ComponentFixture<SinMovimientosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinMovimientosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinMovimientosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
