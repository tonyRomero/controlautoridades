import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SinMovimientosComponent } from './sin-movimientos.component';

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
        {
            path: '',
            component: SinMovimientosComponent
          }
      ])

  ],
  declarations: [SinMovimientosComponent],

  providers : [],
})
export class SinMovimientosModule {

}