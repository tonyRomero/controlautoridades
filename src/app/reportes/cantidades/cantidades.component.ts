import { Component, OnInit } from '@angular/core';
import { Service } from '../../rest-services.service';


@Component({
  selector: 'app-cantidades',
  templateUrl: './cantidades.component.html',
  styleUrls: ['./cantidades.component.css']
})
export class CantidadesComponent implements OnInit {

  usuarios : Promise <any[]>;

  constructor( private service : Service ) { }

  ngOnInit() {

    this.usuarios = this.service.getUsers();
    console.log (this.usuarios);

  }

}
