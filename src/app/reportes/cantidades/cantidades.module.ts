import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CantidadesComponent } from './cantidades.component';

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
        {
            path: '',
            component: CantidadesComponent
          }
      ])

  ],
  declarations: [CantidadesComponent],

  providers : [],
})
export class CantidadesModule {

}