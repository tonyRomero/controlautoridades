import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    },
    {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
    },
    {
        path: 'recupera-contrasenia',
        loadChildren: './recupera-contrasenia/recupera-contrasenia.module#RecuperaContraseniaModule'
    },
    {
        path: 'reporte-cantidades',
        loadChildren: './reportes/cantidades/cantidades.module#CantidadesModule'
    },
    {
        path: 'reporte-porcentajes',
        loadChildren: './reportes/porcentajes/porcentajes.module#PorcentajesModule'
    },
    {
        path: 'reporte-produccion',
        loadChildren: './reportes/produccion/produccion.module#ProduccionModule'
    },
    {
        path: 'admin-usuarios',
        loadChildren: './admin-usuarios/admin-usuarios.module#AdminUsuariosModule'
    },
    {
        path: 'fusion-registros',
        loadChildren: './fusion-registros/fusion-registros.module#FusionRegistrosModule'
    },
    {
        path: 'detalle-usuario',
        loadChildren: './detalle-usuario/detalle-usuario.module#DetalleUsuarioModule'
    }
];