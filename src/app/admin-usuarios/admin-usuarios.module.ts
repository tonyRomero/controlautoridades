import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminUsuariosComponent } from './admin-usuarios.component';

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
        {
            path: '',
            component: AdminUsuariosComponent
          }
      ])

  ],
  declarations: [AdminUsuariosComponent],

  providers : [],
})
export class AdminUsuariosModule {

}