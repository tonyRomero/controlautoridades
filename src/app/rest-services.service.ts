import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams, RequestOptions } from '@angular/http';
import { Http, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { headersToString } from 'selenium-webdriver/http';
import { Headers } from '@angular/http/src/headers';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class Service {

  constructor(
    private http: HttpClient
  ) {}

  private endpoint = 'http://200.38.177.193:8080';
  private httpOptions = { headers: new HttpHeaders()
  .set('Authorization', 'Basic dG9tY2F0OnRvbWNhdA==')
  .set('Content-Type', 'application/json')};

  async getUsers() {

    console.log('Consumiendo servicio...');
    const url = '/rest/admin/getListaUsuarios';
    const body: any = await this.http.post(this.endpoint + url, {}, this.httpOptions )
    .toPromise();
    console.log(body);
    return body;
  }

  postRecuperaContrasenia(email): Observable<Response> {
    
    const url = '/rest/admin/getListaUsuarios';

    let jsonBody = JSON.stringify({email : email});
    
    const body: any = this.http.post(this.endpoint + url, jsonBody, {
      headers: new HttpHeaders()
        .set('Authorization', 'Basic dG9tY2F0OnRvbWNhdA==')
        .set('Content-Type', 'application/json'),
    }).toPromise();

    return body;
  }

  async geTodostUsuarios (){
    const url = '';
    const body : any = await this.http.get (this.endpoint + url, {
      headers: new HttpHeaders()
        .set('Authorization', 'Basic dG9tY2F0OnRvbWNhdA==')
        .set('Content-Type', 'application/json'),
    }).toPromise();

    console.log(body);

    return body;
  }

  async getUsuarioById (id){
    const url = '';
    const body : any = await this.http.post (this.endpoint + url, {id : id}, {
      headers: new HttpHeaders()
        .set('Authorization', 'Basic dG9tY2F0OnRvbWNhdA==')
        .set('Content-Type', 'application/json'),
    }).toPromise();

    console.log(body);

    return body;
  }

  async guardarUsuario (form){
    let jsonBody = JSON.stringify(form);
    const url = '';
    const body : any = await this.http.post (this.endpoint + url, jsonBody, {
      headers: new HttpHeaders()
        .set('Authorization', 'Basic dG9tY2F0OnRvbWNhdA==')
        .set('Content-Type', 'application/json'),
    }).toPromise();
    console.log(body);
    return body;
  }

  async eliminarUsuarioById (id){
    let jsonBody = JSON.stringify({id : id});
    const url = '';
    const body : any = await this.http.post (this.endpoint + url, jsonBody, {
      headers: new HttpHeaders()
        .set('Authorization', 'Basic dG9tY2F0OnRvbWNhdA==')
        .set('Content-Type', 'application/json'),
    }).toPromise();
    console.log(body);
    return body;
  }
 
}