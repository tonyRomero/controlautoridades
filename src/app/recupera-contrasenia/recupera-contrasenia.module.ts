import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { RecuperaContraseniaComponent } from './recupera-contrasenia.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,

    RouterModule.forChild([
        {
            path: '',
            component: RecuperaContraseniaComponent 
          }
      ])

  ],
  declarations: [RecuperaContraseniaComponent],

  providers : [],
})
export class RecuperaContraseniaModule {

}