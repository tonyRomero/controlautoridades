import { Service } from './../rest-services.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-recupera-contrasenia',
  templateUrl: './recupera-contrasenia.component.html',
  styleUrls: ['./recupera-contrasenia.component.css']
})
export class RecuperaContraseniaComponent implements OnInit {

  recuperaForm: FormGroup;
  private respuesta;
  private mostrar: boolean;

  constructor(private fb: FormBuilder, private service: Service) {
    this.recuperaForm = fb.group({
      email: ['',
        Validators.compose([
          Validators.required,
          Validators.email
        ])],
    })
  }

  ngOnInit() {
  }

  recuperarContrasenia(form){
    this.respuesta = this.service.postRecuperaContrasenia(form);
    this.respuesta = 'Recibirá un mensaje en su bandeja de correo con indicaciones para recuperar su contraseña.';
    if (this.respuesta != null){
      this.mostrar = true;
    }
  }
}