import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DetalleUsuarioComponent } from './detalle-usuario.component';


@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
        {
            path: '',
            component: DetalleUsuarioComponent 
          }
      ])

  ],
  declarations: [DetalleUsuarioComponent],

  providers : [],
})
export class DetalleUsuarioModule {

}